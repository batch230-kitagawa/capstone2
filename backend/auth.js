const jwt = require("jsonwebtoken");
const secret = ("eCommerceAPI");

// Token Creator
module.exports.createAccessToken = (user)=> {
	// payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}
return jwt.sign(data, secret, {/*expiresIn: "60s"*/});
//return jwt.sign(data, secret, {expiresIn: "60s"});
}

// Token Verifier
// Verify a token from the request (from Postman)

module.exports.verify = (request,response,next) => {
	// GET JWT (Json Web Token) from Postman
	let token = request.headers.authorization
	if(typeof token !== "undefined"){
		//console.log(token);

		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (error, data)=>{
			if(error){
				return response.send({
					auth: "Failed"
				});
			}
			else{
				next();
			}
		})
	}
}

// Token Decoder
module.exports.decode = (token) => {

	if(typeof token !== "undefined"){

		// remove first 7 characters ("Bearer ") from the token
		token = token.slice(7, token.length);
	}

	return jwt.verify(token, secret, (error, data) => {
		if(error){
			return null
		}
		else{
			return jwt.decode(token, {complete:true}).payload
		}
	})
}
