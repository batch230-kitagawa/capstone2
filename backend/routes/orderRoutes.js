const express = require('express')
const router = express.Router()
const orderControllers = require('../controllers/orderControllers')
const auth = require('../auth')

// NON-ADMIN USER CHECKOUT 
// CREATE ORDER ROUTER
router.post('/checkout', auth.verify, (request, response) => 
{
   orderControllers.createOrder(request).then(resultFromController => response.send(resultFromController));
})

// RETRIEVE ALL ORDERS ROUTER
router.get('/orderHistory', (request, response) => {
    orderControllers.checkOrderHistory(request).then(resultFromController => response.send(resultFromController))
})


// RETRIEVE IN-CART ITEMS
router.get('/viewCart', (request, response) => {
    orderControllers.checkItemsInCart(request).then(resultFromController => response.send(resultFromController))
})

// RETRIEVE ALL ORDERS (ADMIN ONLY)
router.get('/allOrder', auth.verify, (req, res) => {
    const data = auth.decode(req.headers.authorization)

    if (data.isAdmin) {
        orderControllers.getAllOrders().then(resultFromController => res.send(resultFromController))
    }
    else {
        res.send(false)
    }
})


// ADD TO CART ROUTER
router.post('/cart', auth.verify, (request, response) => 
{
    orderControllers.addCart(request).then(resultFromController => response.send(resultFromController));
})


// PLACE AN ORDER OF IN-CART ITEMS
router.get('/placeOrder', (request, response) => {
    orderControllers.placeOrder(request).then(resultFromController => response.send(resultFromController))
})


module.exports = router;