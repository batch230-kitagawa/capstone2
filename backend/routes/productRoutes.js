const express = require('express')
const router = express.Router()
const productControllers = require('../controllers/productControllers')
const auth = require('../auth')


// CREATE PRODUCT ROUTER (ADMIN ONLY)
router.post('/create', auth.verify, (request,response) => 
{
    const result = {
        product: request.body,
        isAdmin: auth.decode(request.headers.authorization).isAdmin
    }
    productControllers.addProduct(request.body, result).then(resultFromController => response.send(resultFromController));
})

// SEARCH ALL PRODUCTS ROUTER
router.get('/searchAll', (request, response) => 
{
	productControllers.findAllProducts().then(resultFromController => response.send
    (resultFromController))
})


// SEARCH ALL ACTIVE PRODUCTS ROUTER
router.get('/searchActive', (request,response) => 
{
    productControllers.findActiveProducts().then(resultFromController => response.send(resultFromController));
})


// SEARCH SINGLE PRODUCT ROUTER
router.get('/findproduct/:productId', (req, res) => {
    console.log(req.params.productId)
    //we can retrieve the course ID by accessing the request's "params" property which contains all the paramteres provided via the URL
    ProductController.GETONEPRODUCT(req.params.productId).then(result => res.send(result));
})



// UPDATE PRODUCT INFO ROUTER (ADMIN ONLY)
router.patch('/:productId/update', auth.verify, (request,response) => 
{
    const newData = {
            product: request.body,
            isAdmin: auth.decode(request.headers.authorization).isAdmin
        }
    productControllers.updateProduct(request.params.productId, newData).then(resultFromController => {
        response.send(resultFromController)
    })
})


// ARCHIVE PRODUCT ROUTER (ADMIN ONLY)
router.patch('/:productId/archive', auth.verify, (request,response) => 
{
	const productId = request.params.productId;
    const isAdmin = auth.decode(request.headers.authorization).isAdmin;

	productControllers.archiveProduct(productId, isAdmin).then(resultFromController => {
		response.send(resultFromController)
	})
})

// UNARCHIVE PRODUCT ROUTER (ADMIN ONLY)
router.patch('/:productId/unarchive', auth.verify, (request,response) => 
{
	const productId = request.params.productId;
    const isAdmin = auth.decode(request.headers.authorization).isAdmin;

	productControllers.unArchiveProduct(productId, isAdmin).then(resultFromController => {
		response.send(resultFromController)
	})
})



module.exports = router;