const express = require('express')
const router = express.Router()
const userControllers = require('../controllers/userControllers')
const auth = require('../auth')


// CHECK EMAIL ROUTER
router.post("/checkEmail", userControllers.checkEmailExists);

// REGISTER USER ROUTER
router.post('/register', (request, response) => {
    userControllers.registerUser(request.body).then(resultFromController => response.send(resultFromController))
})

// LOGIN USER ROUTER
router.post('/login', (request,response) => {
    userControllers.loginUser(request.body).then(resultFromController => response.send(resultFromController))
})

// RETRIEVE USER DETAILS ROUTER
router.get("/details", auth.verify, userControllers.getProfile);






module.exports = router;