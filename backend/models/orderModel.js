// Option 2
// Order
/*
totalAmount - number,
purchasedOn - date
		     default: new Date(),
userId - string
products - [
	{
		productId - string,
		quantity - number
	}
]
*/

const mongoose = require('mongoose');
const orderSchema = new mongoose.Schema({
    userId: {
        type: String,
        required: [true, "User Id is required"]
    },

    isOrderPlaced: {
        type: Boolean,
        required: true,
        default: false
    },

    totalAmount: {
        type: Number,
        required: true
    },

    purchasedOn: {
        type: Date,
        default: new Date()
    },

    products: [
        {
            productId: {
                type: String,
                required: [true, "Product Id is required"]
            },
            
            quantity: {
                type: Number,
                required: [true, "Input quantity"]
            }
        }
    ]
})

module.exports = mongoose.model('Order', orderSchema);