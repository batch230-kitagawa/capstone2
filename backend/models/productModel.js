// Option 2

// Product
/*
name - string,
description - string,
price - number,
stocks - number,
isActive - boolean
		   default: true,
createdOn - date
			default: new Date()
*/

const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Please input product name"]
    },

    description: {
        type: String,
        required: [true, "Please input description"]
    },

    price: {
        type: Number,
        required: [true, "Please input price"]
    },

    stock: {
        type: Number,
        default: 0
    },

    isActive: {
        type: Boolean,
        default: true
    },

    createdOn: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Product', productSchema);