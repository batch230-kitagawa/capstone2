// Dependencies
const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const productRoutes = require('./routes/productRoutes');
const userRoutes = require('./routes/userRoutes');
const orderRoutes = require('./routes/orderRoutes');
const port = process.env.PORT || 3000


// To create an express application
const app = express();

// To allow cross origin resource sharing
app.use(cors());
// To read json objects
app.use(express.json());
// To read forms
app.use(express.urlencoded({extended:true}));
app.use('/products', productRoutes);
app.use('/users', userRoutes);
app.use('/orders', orderRoutes);


// To make a connection 
mongoose.connect("mongodb+srv://admin:admin@batch230.y9m8uao.mongodb.net/KitagawaE-commerce?retryWrites=true&w=majority", {
    useNewUrlParser: true,
    useUnifiedTopology: true
})

// To get reference to database
const db = mongoose.connection;
db.on('error', console.error.bind(console, "connection error:"));
db.once('open',() => console.log("Connection Successful!"));


app.listen(process.env.PORT || 3000, () => {
	console.log(`Server started on port ${process.env.PORT || 3000}`)
});

