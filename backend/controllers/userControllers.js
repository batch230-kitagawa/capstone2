const User = require('../models/userModel')
const Order = require('../models/orderModel')
const Product = require('../models/productModel')
const auth = require('../auth')
const bcrypt = require('bcrypt')


// CHECK EMAIL
// @route POST /users/checkEmail
module.exports.checkEmailExists = (req, res) =>{
	return User.find({email: req.body.email}).then(result =>{

		// The result of the find() method returns an array of objects.
			 // we can use array.length method for checking the current result length
		console.log(result);

		// The user already exists
		if(result.length > 0){
			return res.send(true);
			// return res.send("User already exists!");
		}
		// There are no duplicate found.
		else{
			return	res.send(false);
			// return res.send("No duplicate found!");
		}
	})
	.catch(error => res.send(error));
}

// REGISTER USER
// @route POST /users/register
module.exports.registerUser = (reqBody) => {
    let newUser = new User({
        firstName: reqBody.firstName,
        lastName: reqBody.lastName,
        email: reqBody.email,
        mobileNumber: reqBody.mobileNumber,
        password: bcrypt.hashSync(reqBody.password, 10)

    })

    return newUser.save().then((user,error)=> {
        if(error){
            return false;
        }
        else{
            return true;
        }
    })
}

// LOGIN USER
// @route POST /users/login
module.exports.loginUser = (reqBody) => {
    return User.findOne({email: reqBody.email}).then(result => {
      //console.log(result); 
        if(result == null){
            return false;
        }
        else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
            if(isPasswordCorrect){
                return{access: auth.createAccessToken(result)};
            }
            else{
                return false;
            }

        }
    })
}

// RETRIEVE USER DETAILS
// @route GET /users/details
module.exports.getProfile = (req, res) => {
		
    const userData = auth.decode(req.headers.authorization);

    console.log(userData);

    return User.findById(userData.id).then(result =>{
        result.password = "***";
        res.send(result);
    })
}