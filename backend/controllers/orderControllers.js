const Product = require('../models/productModel')
const Order = require('../models/orderModel')
const User = require('../models/userModel')
const auth = require('../auth')

// NON-ADMIN USER CHECK-OUT
// CREATE ORDER
// @route POST /orders/checkout
module.exports.createOrder = async (request, response) => {
    
    const userData = auth.decode(request.headers.authorization);
    console.log("Debug point 4", request.body);
    let newOrder = new Order({
        userId: userData.id,
        totalAmount: request.body.totalAmount,
        isOrderPlaced: true,
        products: {
            productId: request.body.productId,
            quantity: request.body.quantity
        }  
    })

    let isOrderCreated = await newOrder.save()
    .then(result => {
        console.log(result);
        return true;
    })
    .catch(error => {
        console.log(error);
        return false;
    })
    console.log("Debug point 5", isOrderCreated);

    let isProductUpdated = await Product.findById(request.body.productId)
    .then(product => {

		product.stock -= request.body.quantity;

		return product.save()
		.then(result=>{
			console.log(result);
			return true;
		})
		.catch(error => {
			console.log(error);
			return false;
		})
	})
    console.log(isProductUpdated);

    if(isOrderCreated == true &&  isProductUpdated == true){
        return true;
    }
    else{
       console.log("2");
        return false;
    }


}


// RETRIEVE AUTHENTICATED USER'S ORDERS
// @route GET /orders/orderHistory
module.exports.checkOrderHistory = (request) => {
    const id = auth.decode(request.headers.authorization).id;
    return Order.find({userId: id}, {isOrderPlaced: true}).then(result => {
        return result;
    })
}

// RETRIEVE AUTHENTICATED USER'S IN-CART ITEMS
// @route GET /orders/viewCart
module.exports.checkItemsInCart = (request) => {
    const id = auth.decode(request.headers.authorization).id;
    return Order.find({userId: id}, {isOrderPlaced: false}).then(result => {
        return result;
    })
}


// RETRIEVE ALL ORDERS (ADMIN)
// @route GET /orders/allOrder
module.exports.getAllOrders = (reqBody) => {
    return User.find({}).then(result => {
        return result.orders
    })
}


// ADD TO CART
//@ route POST /orders/cart
/*module.exports.addToCart = async (data) => {
    let isAddedToCart = await User.findById(data.userId).then(user => {
        user.cart.push({productId : data.productId});

        const userData = auth.decode(request.headers.authorization);

        let newOrder = new Order({
            userId: userData.id,
            totalAmount: request.body.totalAmount,
            products: {
                productId: request.body.productId,
                quantity: request.body.quantity
            }  
        })

        return user.save().then ((user, error) => {
            if(error) {
                return false;
            } else {
                return true, 'Product added to cart'
            }
        })
    })
}
*/

// NON-ADMIN USER CHECK-OUT
// ADD CART
// @route POST /orders/cart
module.exports.addCart = (request, response) => {
    
    const userData = auth.decode(request.headers.authorization);

    let newOrder = new Order({
        userId: userData.id,
        isOrderPlaced: false,
        totalAmount: request.body.totalAmount,
        products: {
            productId: request.body.productId,
            quantity: request.body.quantity
        }  
    })

    return newOrder.save().then ((result, error) => {
        if(error) {
            return false;
        } else {
            return true; /*Product added to cart*/
        }
    })

}

// NON-ADMIN USER CHECK-OUT
// PLACE ORDER
// @route POST /orders/placeOrder
module.exports.placeOrder = async (request, response) => {
    
    let isOrderUpdated = await Order.findById(request.body.orderId)
        .then(order => {

            order.isOrderPlaced = true;
            return order.save()
            .then(result=>{
                console.log(result);
                return true;
            })
            .catch(error => {
                console.log(error);
                return false;
            })
        })
        console.log(isOrderUpdated);
    
        let isProductUpdated = await Product.findById(request.body.productId)
        .then(product => {
    
            product.stock -= request.body.quantity;
    
            return product.save()
            .then(result=>{
                console.log(result);
                return true;
            })
            .catch(error => {
                console.log(error);
                return false;
            })
        })
    console.log(isProductUpdated);
    
    if(isOrderUpdated == true &&  isProductUpdated == true){
        return true;
    }
    else{
       console.log("2");
        return false;
    }

}
