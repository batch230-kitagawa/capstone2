const Product = require('../models/productModel')
const Order = require('../models/orderModel')
const User = require('../models/userModel')
const auth = require('../auth')

// CREATE PRODUCT (ADMIN ONLY)
// @route POST /products/create
module.exports.addProduct = (reqBody, result) => {
	if(result.isAdmin == true){
	let newProduct = new Product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price,
		stock: reqBody.stock
	})
	return newProduct.save().then((newProduct, error) =>
		{
			if(error){
				return error;		
			}
			else{
				return newProduct;
			}
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// SEARCH ALL PRODUCTS
// @route GET /products/searchAll
module.exports.findAllProducts = () => {
	return Product.find({}).then(result =>{
		return result;
	})
}


// SEARCH ALL ACTIVE PRODUCTS
// @route GET /products/searchActive
module.exports.findActiveProducts = () => {
    return Product.find({isActive:true}).then(result => {
        return result;
    })
}

// SEARCH SINGLE PRODUCT
// @route POST /products/search
//RETRIEVE A SINGLE PRODUCT
module.exports.GETONEPRODUCT = (getOneProduct) => {
	return Product.findById(getOneProduct).then((found,notFound) => {
			if(found) {
				return found;
			} else {
				return ({Message:"Product does not exist."});
			};
		}).catch(error => error)
}

// UPDATE PRODUCT INFORMATION (ADMIN ONLY)
// @route PATCH /products/id/update
module.exports.updateProduct = (productId, newData) => {
	if(newData.isAdmin == true){
		return Product.findByIdAndUpdate(productId,
			{
		name: newData.product.name,
		description: newData.product.description,
		price: newData.product.price,
		stock: newData.product.stock
	}).then((result, error)=> {
		if(error){
			return false;
		}
		return true;
	})
}
else{
	let message = Promise.resolve('User must be Admin to access this funstionality');
	return message.then((value) => {
		console.log(value)
		return value
	});
}
}


// ARCHIVE PRODUCT (ADMIN ONLY)
// @route PATCH /products/id/archive
module.exports.archiveProduct = (productId, isAdmin) => {
	if(isAdmin == true){
		return Product.findByIdAndUpdate(productId,
				{ isActive: false }
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}


// UNARCHIVE PRODUCT (ADMIN ONLY)
// @route PATCH /products/id/unarchive
module.exports.unArchiveProduct = (productId, isAdmin) => {
	if(isAdmin == true){
		return Product.findByIdAndUpdate(productId,
				{ isActive: true }
		).then((result, error)=>{
			if(error){
				return false;
			}
			return true
		})
	}
	else{
		let message = Promise.resolve('User must be ADMIN to access this functionality');
		return message.then((value) => {return value});
	}
}



